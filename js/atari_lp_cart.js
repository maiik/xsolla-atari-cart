//подсчет скидки на количество
var quantityDiscount = false;

var debug = false; //вывод всяких сообщений в консоль


//Без скидочных цен
var atari_custom_attributes1 = {
  'atari_blade_runner': {
    'shipping_enabled': true,
  },
  'atari_black': {
    'shipping_enabled': true,
  },
  'atari_royal_blue': {
    'shipping_enabled': true,
  },
  'atari_fuji': {
    'shipping_enabled': true,
  },
  'atari_pong': {
    'shipping_enabled': true,
  },
}



//Со скидочными ценами
var atari_custom_attributes = {
  'atari_blade_runner': {
    'shipping_enabled': true,
    'oldpriceUSD': 139.99,
    'oldpriceEUR': 159.99,
  },
  'atari_black': {
    'shipping_enabled': true,
    'oldpriceUSD': 129.99,
    'oldpriceEUR': 149.99,
  },
  'atari_royal_blue': {
    'shipping_enabled': true,
    'oldpriceUSD': 129.99,
    'oldpriceEUR': 149.99,
  },
  'atari_fuji': {
    'shipping_enabled': true,
    'oldpriceUSD': 129.99,
    'oldpriceEUR': 149.99,
  },
  'atari_pong': {
    'shipping_enabled': true,
    'oldpriceUSD': 129.99,
    'oldpriceEUR': 149.99,
  },
}


var sale = false;

//true — показывает зачеркнутые цены
//true — Скрывает блок с дополнительными промо в корзине
//выставляется автоматически, если есть старые цены


//Sale определяется, есть ли разница в цене хоть у одного товара
function addStaticPrices_callback(el, elData, currency) { //Сравнивать цены
  try {
    $('[data-good-sku=\'' + elData.sku  + '\']').each(function (i, item) {

      var oldpriceKey = 'oldprice' + currency;

      if (!elData[oldpriceKey] || elData[oldpriceKey] === elData['amount']) { //  || elData[oldpriceKey] === elData['amount']
        $(item).parent('.x_offer_price').addClass('hidden');
        $(item).parent().siblings('.price_normal').removeClass('invisible');
        $(item).parent().siblings().find('.x_prbuy_offer').addClass('hidden');
      } else {
        $(item).parent('.x_offer_price').removeClass('hidden');
        $(item).parent().siblings('.price_normal').removeClass('invisible');
        $(item).parent().siblings().find('.x_prbuy_offer').removeClass('hidden');

        sale = true;
        if (debug) console.log('sale = ', sale);
      }
    })
  } catch (e) {}

}







//Эти идут
var shopSettings_Atari =
{
  // 'theme': 'xxx_theme_light',


    'shopSettings': {
      'noDynamicStoreftont': true,
    },

  'cartSettings': {
    'cartCheckoutBut': '.button_buy--checkout--big',
    'indicatorShown': 'auto', //auto
    'cartHideAnim': ixCartHide,
    'cartOpenAnimation': addBodyNoScroll,
    'cartCloseAnimation': removeBodyNoScroll,
    'cartShown': 'default', //auto
    'afterChange': typeof kCheckOutPosition !== 'undefined' ? kCheckOutPosition : null, //Callback when something changed in the cart
    // 'afterChange': typeof showCartNoti !== 'undefined' ? showCartNoti : null, //Callback when something changed in the cart
    'cartAddAnimation': typeof cartAddAnimation !== 'undefined' ? cartAddAnimation : null,
    'cartAddDiscount': typeof cartAddDiscount !== 'undefined' ? cartAddDiscount : null,
    'paystation': {
      'access_data': {
        'user': {
          'attributes': {
            // 'promo': false,
          }
        },
        'settings': {
          'project_id': 22887,
          // 'shipping_enabled': true
          'ui': {
            'size': 'medium',
            // 'theme': 'dark'
          },
        },
      },
    },
    'currency': ['$', 0],
  },
  'customAttrs': atari_custom_attributes //Custom Pictures and shipping
};








function atariShowHidesale() {
  var promoBlock = document.querySelector('#atari_discount');
  var promoLine = document.querySelector('#atari_discount_line');
  var upsaleBlock = document.querySelector('#atari_upsale');

  if (quantityDiscount) { //Бандл-дисконт есть
    promoBlock.classList.remove('hidden');
    promoLine.classList.remove('hidden');
    upsaleBlock.classList.add('hidden');
    // $('.x_offer_price').addClass('hidden');
    // $('.x_prbuy_offer').addClass('hidden');

  } else { //Бандл-дисконтов нет
    promoBlock.classList.add('hidden');
    promoLine.classList.add('hidden');
    upsaleBlock.classList.remove('hidden');
    // $('.x_prbuy_offer').removeClass('hidden');
    if (xsolla.shop.currency[0] !== 'USD') {
      // $('.x_offer_price').addClass('hidden');
    } else {
      // $('.x_offer_price').removeClass('hidden');
    }
  }
  // $('.price_normal').removeClass('invisible');
}







var ix_But_addToCart_1 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '-102px',
  }]
};

var ix_But_addToCart_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '0px',
  }]
};


var ix_But_addBuy_1 = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};

var ix_But_addBuy_0 = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
  }]
};



// var kartItemInitialHeight = 80;



function cartAddAnimation(el, already) {
  var butCart = el.parentElement.parentElement.querySelector('.ix_bt_cart');
  var carticon = el.parentElement.parentElement.querySelector('.ix_bt_carticon');
  var butBuy = el.parentElement.parentElement.parentElement.querySelector('.ix_bt_buy')
  var crtdelicon = el.parentElement.parentElement.querySelector('.ix_bt_crtdelicon');

  if (el.classList.contains('k_li_upsale_item')) return;
  if (already) {
    // this.element.parentElement.parentElement.classList.add('already');
    ix.run(ix_But_addToCart_1, butCart);
    ix.run(ix_But_addBuy_1, butBuy);
    carticon.classList.add('hidden');
    crtdelicon.classList.remove('hidden');
    setTimeout(function() {
      butBuy.classList.add('invisible');
    }, 50);
  } else {
    // this.element.parentElement.parentElement.classList.remove('already');
    ix.run(ix_But_addToCart_0, butCart);
    ix.run(ix_But_addBuy_0, butBuy);
    crtdelicon.classList.add('hidden');
    carticon.classList.remove('hidden');
    // setTimeout(function() {
      butBuy.classList.remove('invisible');
    // }, 200);
  }
}



function cartAddDiscount(thiss) { //TODO: сделать подсчет скидки более универсальным, передавать не кол-во, а состав корзины

  var q = thiss.q;
  var total = thiss.total;

  if (!quantityDiscount) return [0, 0]; //TODO: отключение скидки

  var discount = 0;
  var offvalue = false;
  var discountBlock = document.querySelector('#kart_discount');
  $(discountBlock).find('.active').removeClass('active').addClass('inactive');
  // if (this.cartCont.querySelector('#kart_discount')) {
  if (q < 2) {
    // this.cartCont.removeChild(this.cartCont.querySelector('#kart_discount'));
    discount = 0;
    offvalue = false;
  }
  if (q === 2) {
    discount = (total * (10 / 100)) * -1; //10% от тотала
    $(discountBlock).find('[data-cart-discount="' + 10 + '"]').addClass('active').removeClass('inactive');
    // offvalue = '15OFF';
    offvalue = false;
  }
  if (q >= 3) {
    discount = (total * (20 / 100)) * -1; //20% от тотала
    // $(this.discountBlock).find('.active').addClass('active');
    $(discountBlock).find('[data-cart-discount="' + 20 + '"]').addClass('active').removeClass('inactive');
    // offvalue = '25OFF';
    offvalue = false;
  }
  if (debug) console.log('discount = ', discount);
  return [discount, offvalue];
}


var notiShown = false;
function showCartNoti(thiss) {
  var notiEl = $(':attr(\'^data-kart-noti\')');
  var ixNotiHide = {
    'stepsA': [{
      'opacity': 1,
      'transition': 'transform 200ms ease 0ms, opacity 250ms ease 0ms',
      'x': '0px',
      'y': '80px',
      'z': '0px',
    }],
    'stepsB': []
  };

  if (thiss['q'] > 0) {
    ix.run(ixResetXY, notiEl);
  } else {
    ix.run(ixNotiHide, notiEl);
  }
}




function kCheckOutPosition() {
  // if (!isMobile) {
  //   setTimeout(function () {
  //     // document.querySelector('.k _bot').style.top = document.querySelector('.kb').offsetHeight + 40 - document.querySelector('.k_bot').offsetHeight + 'px';
  //     var kBotTop = document.querySelector('.k_bot').getBoundingClientRect().top;
  //     var kbCont = document.querySelector('.kb_cont');
  //     var newHeight = kBotTop - kbCont.getBoundingClientRect().top;
  //     console.log('newHeight = ', newHeight);
  //     kbCont.style.height = newHeight + 'px';
  //   }, 50);
  // }
    // console.log('resize');
    // alert('resize');
}

$( window ).resize(function() {
  kCheckOutPosition();
});










var ixKPlus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    'y': '-26px',
  }]
};



// var ixKPlusOff = {
//   'stepsA': [{
//     'opacity': 1,
//     'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
//     'scaleX': 1,
//     'scaleY': 1,
//     'scaleZ': 1,
//     'y': '0px',
//   }]
// };

var ixKMinus = {
  'stepsA': [{
    'opacity': 1,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'scaleX': 1,
    'scaleY': 1,
    'scaleZ': 1,
    'y': '26px',
  }]
};

// var ixKPlusMinusOff = {
//   'stepsA': [{
//     'opacity': 0,
//     'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
//     'scaleX': 1,
//     'scaleY': 1,
//     'scaleZ': 1,
//     // 'y': '0px',
//   }]
// };


var ixCartHide = {
  'stepsA': [{
    'transition': 'transform 200 ease 0, opacity 200ms ease 0ms',
    'x': '0px',
    'y': '20px',
    'z': '0px',
    'opacity': 0,
  }],
  'stepsB': []
}


var ix_qChange_1 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};
var ix_qChange_0 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 1,
    'transition': 'transform 100ms ease 0ms, opacity 200ms ease 0ms',
  }]
};


// var dataStatic = phoenix_data;





// var access_data_phoenix = {
  //   "settings": {
    //     "project_id": 24042
    //   }
    // };

    // init(access_data_atari);
    // init(access_data_phoenix);



    //22877




////////////////////   PANEL   //////////////////////

qq(true);
var xsolla;
xsolla = new Xsolla(shopSettings_Atari, atariShowHidesale); //Callback
// xsolla = new Xsolla(shopSettings_Atari); //Callback



function qq(hide) {
  // if (!xsolla) xsolla = new Xsolla(shop);
  if (hide) {
    $('.q').removeClass('shown');
    $('.b').removeClass('xsolla_panel');
    $('.float').removeClass('xsolla_panel');
    return
  }
  $('.q').addClass('shown');
  $('.b').addClass('xsolla_panel');
  $('.float').addClass('xsolla_panel');

}




$(':attr(\'^data-q-close\')').click(function () {
  qq(true)
})


// $('.k').removeClass('shown');










//atariShowHidesale();













function currentScroll() {
  return window.scrollY;
}



var currScroll = 0;


function addBodyNoScroll() {

  currScroll = currentScroll();
  // $('.xxx').addClass('hidden');
  $('.xxx').addClass('noscroll');

  // var inner_clone = $('#xxx_inner_original')[0].cloneNode(true);
  // inner_clone.id = 'xxx_inner_clone'

  // $('#xxx_inner_original')[0].classList.add('xxx_inner_original');

  // inner_clone.classList.add('xxx_inner_clone');
  // inner_clone.style.marginTop = currScroll * -1 + 'px';
  // $('.xxx')[0].appendChild(inner_clone);

  var inner_original = $('#xxx_inner_original')[0];
  inner_original.classList.add('xxx_inner_clone');
  inner_original.style.marginTop = currScroll * -1 + 'px';

  $('body').removeClass('k_closed');
  if (isMobile) {
    $('html')[0].scrollTop = 0;
  }

}




function removeBodyNoScroll() {
  // if (!$('.p').hasClass('shown') && !$('.k').hasClass('shown')) {
  // document.querySelector('.xxx').classList.remove('hidden');
  // $('.xxx').removeClass('hidden');


  // $('#xxx_inner_clone')[0].parentElement.removeChild($('#xxx_inner_clone')[0]);
  $('#xxx_inner_original')[0].classList.remove('xxx_inner_clone');


  // setTimeout(function() {
    $('.xxx').removeClass('noscroll');
    // if (isMobile) {
    $('body').addClass('k_closed');
    // }
    // }, 100);
    $('#xxx_inner_original')[0].style.marginTop = '0px';
    window.scrollTo(0, currScroll);

  setTimeout(function(){
    $('.v4_h').css('transform', 'translateX(0px) translateY(0px) translateZ(0px) scaleX(1) scaleY(1) scaleZ(1)');
    $('.v4_h, .v4_bg').css('opacity', 1);
  }, 200);

  // $('.xxx_inner').css('margin-top', '0px');
  // $('html, body').animate({ scrollTop: currentScroll() }, 0);

}







// $(':attr(\'^data-pop\')').on({
//   click: function (evt) {

//     evt.stopPropagation();
//     var el = evt.target;
//     while (!el.classList.contains('button_learn_more') ) {
//       el = el.parentElement;
//     }

//     var pop = el.dataset.pop;

//     console.log('pop = ', pop);

//     $('.p').addClass('shown');
//     $('.p_cont').removeClass('shown');
//     $('.' + pop).addClass('shown');

//     addBodyNoScroll(true);

//   }
// });



// $('.kz, .k_top_close, .k_top_clear').click(function (evt) { //TODO: сделать лучше
//   evt.stopPropagation();
//   $('.p').removeClass('shown');
//   // $('.k').removeClass('shown');
//   removeBodyNoScroll();
// });













var ix_cartHide = {
  'stepsA': [{
    'opacity': 0,
    'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
    'x': '00px',
    'y': '-30px',
    'z': '0px'
  }],
  'stepsB': []
}

var ix_addGlow_1 = {
  'stepsA': [{
    'opacity': 1,
    'scale': 0.5,
    'transition': 'transform 300ms ease 0ms, opacity 300ms ease 0ms',
  }]
};




// $('.scrolltop').on({
//   click: function (evt) {
//     $('html, body').animate({ scrollTop: $('.c').offset().top }, 200);
//   }
// });



// $('.v4_subsbut').on({
//   click: function (evt) {
//     $('html, body').animate({ scrollTop: $('#subscribe').offset().top - 60 }, 200);
//   }
// });


// $(':attr(\'^data-scrollto\')').on({
//   click: function (evt) {
//     $('html, body').animate({ scrollTop: $('.o--specs').offset().top }, 200);
//   }
// });



// $(':attr(\'^data-kart-open\')').on({
//   click: function (evt) {
//     evt.stopPropagation();
//     addBodyNoScroll();
//   }
// });

// $(':attr(\'^data-kart-close\')').on({
//   click: function (evt) {
//     evt.stopPropagation();
//     removeBodyNoScroll();
//   }
// });


// $(':attr(\'^data-kart-close\')').each(function (i, item) {
//   item.addEventListener('click', function (evt) {
//     // evt.stopPropagation();
//     //$('.k').removeClass('shown');
//     // $('.k').removeClass('shown');
//     removeBodyNoScroll();
//   });
// });









var twttr;

var Webflow = Webflow || [];
Webflow.push(function () {

  // Twitter widget is loading.
  twttr = function (d, s, id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
    js.src = 'https://platform.twitter.com/widgets.js';
    fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
    }(document, 'script', 'twitter-wjs');

    // When widget is ready, run masonry
    twttr.ready(function (twttr) {
      twttr.events.bind('loaded', function (event) {
          $('.swiper-tweets').each(function(i, obj) {
            var swipes = [];
            swipes[i] = Swiper(obj, {
              pagination: '.swiper-pagination',
              slidesPerView: 'auto',
              paginationClickable: true,
              setWrapperSize: true,
              nextButton: '.swiper-button-next',
              prevButton: '.swiper-button-prev',
            });
          });

      });
    });

});





addScrollTo();