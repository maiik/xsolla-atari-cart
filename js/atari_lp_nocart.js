
jQuery.expr.pseudos.attr = $.expr.createPseudo(function (arg) {
  var regexp = new RegExp(arg);
  return function(elem) {
    for(var i = 0; i < elem.attributes.length; i++) {
      var attr = elem.attributes[i];
      if(regexp.test(attr.name)) {
        return true;
      }
    }
    return false;
  };
});




// $('.swiper_page').each(function(i, obj) {
//   var swipes = [];
//   swipes[i] = Swiper(obj, {
//     pagination: '.swiper-pagination',
//     // slidesPerView: 'auto',
//     slidesPerView: 'auto',
//     centeredSlides: true,
//     spaceBetween: 0,
//     loopedSlides: true,
//     setWrapperSize: true,
//     nextButton: '.swiper-button-next',
//     prevButton: '.swiper-button-prev',
//     //slideToClickedSlide: true,
//     // onTransitionEnd(this) {
//     //   console.log(this)
//     // }
//   });
// });


// $('.swiper--pop').each(function (i, obj) {
//   var swipes = [];
//   swipes[i] = Swiper(obj, {
//     slidesPerView: 1,
//     // paginationClickable: true,
//     pagination: '.swiper-pagination',
//     // spaceBetween: 0,
//     // loopedSlides: true,
//     //lazyLoading: true,
//     setWrapperSize: true,
//     // autoHeight: true,
//     height: 200,
//     // width: 320,
//     onTransitionStart: function (s) {
//       // selectTop(s.activeIndex)
//     },
//     // effect: 'fade',
//     onTransitionEnd: function (s) {
//       // changeTheme(s.activeIndex);
//     },


//     // autoplay: 2000,
//   }
//   )
// }
// );

// var myPrSwiperThumbs = new Swiper('.swiper_product_thumb', {
//   slidesPerView: 'auto',
//   slideToClickedSlide: true,
//   touchRatio: 1,
//   // preventClicksPropagation: true,
//   //centeredSlides: true,
//   // onClick: function (s) {
//   //   console.log('s.activeIndex = ', s.activeIndex);
//   // }
//   // paginationClickable: true,
//   //        spaceBetween: 0,
//   //        loopedSlides: false,
//   //setWrapperSize: true,
//   // onTransitionEnd: function (s) {
//   //   selectTop(s.activeIndex)
//   // }
// });
// myPrSwiper.params.control = myPrSwiperThumbs;
// myPrSwiperThumbs.params.control = myPrSwiper;


$('.scrolltop').on({
  click: function (evt) {
    $('html, body').animate({ scrollTop: $('.c').offset().top }, 200);
  }
});



$('.v4_subsbut').on({
  click: function (evt) {
    $('html, body').animate({ scrollTop: $('#subscribe').offset().top - 60 }, 200);
  }
});


$(':attr(\'^data-scrollto\')').on({
  click: function (evt) {
    $('html, body').animate({ scrollTop: $('.o--specs').offset().top }, 200);
  }
});






$(':attr(\'^data-pr\')').on({
  click: function (evt) {

    //find li
    evt.stopPropagation();
    var el = evt.target;


    while (!el.classList.contains('s_g') ) {
      el = el.parentElement;
    }
    if (el.classList.contains('swiper-slide-active')) return;

    var newSl = el.dataset.pr;
    newSl = parseInt(newSl);
    console.log('newSl = ', newSl);

    myPrSwiper.slideTo(newSl);

    //mark active
    // $(el).parent().parent().children().removeClass('swiper-slide-active');
    $('.swiper-slide--thumb').removeClass('swiper-slide-active');
    el.parentElement.classList.add('swiper-slide-active');

  }
});



var atarilinks = true;


$(':attr(\'^data-pop\')').on({
  click: function (evt) {

    evt.stopPropagation();
    var el = evt.target;
    while (!el.classList.contains('button_learn_more') ) {
      el = el.parentElement;
    }
    var pop = el.dataset.pop;

    if (atarilinks) {

      // window.location = pop + '.html';
      window.location = pop + '/';
      return;
    }
    ///////////////////////////////////////////////////////////////////////



    console.log('pop = ', pop);

    $('.p').addClass('shown');
    $('.p_cont').removeClass('shown');
    $('.' + pop).addClass('shown');

    addBodyNoScroll(true);

  }
});




// $('.bx_but').on({
//   click: function (evt) {
//     evt.stopPropagation();
//     if (atarilinks) {
//       // window.location = pop + '.html';
//       window.location = '/';
//       return;
//     }
//   }
// });



$(':attr(\'^data-buy\')').on({
  click: function (evt) {

    evt.stopPropagation();
    var el = evt.target;
    while (!el.classList.contains('button_buy') ) {
      el = el.parentElement;
    }

    var sku = el.dataset.buy;

    console.log('sku = ', sku);

    //if (!debug) {
    //  $('.x_bundle').removeClass('shown');
    //} else {
    //  $('.x_bundle').addClass('shown');
    //}
    $('.x_bundle').removeClass('shown');
    $('.' + sku).addClass('shown');

    addBodyNoScroll(true);
  }
});

var debug = false;


function selectTop(ind) {
  // $(':attr(\'^data-pr\')').removeClass('active');
  // console.log('ind = ', ind);
  // $('.swiper-slide--thumb').removeClass('swiper-slide-active');
  // $('*[data-pr='' + ind + '']')[0].parentElement.classList.add('swiper-slide-active');
}



var isMobile = isMobile || false;
(function () {
  $('.p').removeClass('shown');
  $('.p').removeClass('init');
  $('.x').removeClass('shown');


if($('.ismobile').css('display')=='none') {
      isMobile = false;
  }
})();




function addBodyNoScroll(partially) {
  currentScroll = window.scrollY;

  if (partially) {
    $('body').addClass('noscroll');
    return;
  }
  $('.xxx').addClass('hidden');
  if (isMobile) {
    $('body').removeClass('k_closed');
  }
}


var currentScroll = 0;

function removeBodyNoScroll() {
  $('body').removeClass('noscroll');
  $('.xxx').removeClass('hidden');
  if (isMobile) {
    $('body').addClass('k_closed');
  }
  $('html, body').animate({ scrollTop: currentScroll }, 100);
}



function removeBodyNoScrollOld() {
  $('body').removeClass('noscroll');
  if (!$('.p').hasClass('shown') && !$('.k').hasClass('shown')) {
    $('body').css({ 'overflow': 'auto', 'position': 'relative', 'height': 'auto' });
  }
}



function addBodyNoScrollOld(fix) {
  // $('body').addClass('noscroll');

  if (isMobile && fix) {
    $('body').css({ 'overflow': 'hidden', 'position': 'fixed', 'height': '100%' });
  } else {
    $('body').css({ 'overflow': 'hidden', 'position': 'static'});
  }

  // $('body')[0].ontouchmove = function (e) {
  //   e.preventDefault();
  // }

  // $('.o--pop').each(function (i, obj) {
  //   obj.ontouchmove = function (e) {
  //     e.preventDefault();
  //   } }
  // )

  // $('.pp').each(function (i, obj) {
  //   obj.ontouchmove = function (e) {
  //     return true;
  //   } }
  // )

}

// window.blockMenuHeaderScroll = false;
// $(window).on('touchstart', function(e)
// {
//     if ($(e.target).closest('.o--pop').length == 1)
//     {
//         blockMenuHeaderScroll = true;
//     }
// });
// $(window).on('touchend', function()
// {
//     blockMenuHeaderScroll = false;
// });
// $(window).on('touchmove', function(e)
// {
//     if (blockMenuHeaderScroll)
//     {
//         e.preventDefault();
//     }
// });

  // document.body.addEventListener('touchmove',function(e)
  // {
  //     e = e || window.event;
  //     var target = e.target || e.srcElement;
  //     //in case $altNav is a class:
  //     if (target.className.match(/\bo--pop\b/))
  //     {
  //       console.log('!');
  //         e.returnValue = false;
  //         e.cancelBubble = true;
  //         if (e.preventDefault)
  //         {
  //             e.preventDefault();
  //             e.stopPropagation();
  //         }
  //         return false;//or return e, doesn't matter
  //     }
  //     //target is a reference to an $altNav element here, e is the event object, go mad
  // },false);





var twttr;

var Webflow = Webflow || [];
Webflow.push(function () {


  // Twitter widget is loading.
  twttr = function (d, s, id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js = d.createElement(s); js.id = id;
    js.src = 'https://platform.twitter.com/widgets.js';
    fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } });
    }(document, 'script', 'twitter-wjs');



    // When widget is ready, run masonry
    twttr.ready(function (twttr) {
      twttr.events.bind('loaded', function (event) {

          $('.swiper-tweets').each(function(i, obj) {
            var swipes = [];
            swipes[i] = Swiper(obj, {
              pagination: '.swiper-pagination',
              //scrollbar: '.swiper-scrollbar',
              // scrollbarHide: false,
              // scrollbarDraggable: true,
              // scrollbarSnapOnRelease: true,
              slidesPerView: 'auto',
              paginationClickable: true,
              // autoHeight: true,
              // centeredSlides: true,
              // spaceBetween: 20,
              // loopedSlides: true,
              setWrapperSize: true,
              nextButton: '.swiper-button-next',
              prevButton: '.swiper-button-prev',
              //slideToClickedSlide: true,
              // onTransitionEnd(this) {
              //   console.log(this)
              // }
            });
          });


      });
    });



});





var atari = {

  'atari_fuji': {
    'name': 'Fuji Blackout',
    'pic': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59cafbf7899ef10001054c3c_200_fuji.png',
    'price': 129.99
  },
  'atari_blade_runner': {
    'name': 'Blade Runner 2049',
    'pic': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59cafbf75fbcd60001319745_200_blader.png',
    'price': 139.99
  },
  'atari_royal_blue': {
    'name': 'Royal Blue/White',
    'pic': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59c70c0dadda34000132a224_blue_01.jpg',
    'price': 129.99
  },
  'atari_black': {
    'name': 'Black/White',
    'pic': 'https://daks2k3a4ib2z.cloudfront.net/59c064a86eb47c0001393eae/59c70c0a84228d0001781820_black_01.jpg',
    'price': 129.99
  },
}








var ix = ix || Webflow.require('ix');

// var ixKPlus = {
//   'stepsA': [{
//     'opacity': 1,
//     'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
//     'scaleX': 1,
//     'scaleY': 1,
//     'scaleZ': 1,
//     'y': '-26px',
//   }]
// };

// var ixKMinus = {
//   'stepsA': [{
//     'opacity': 1,
//     'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
//     'scaleX': 1,
//     'scaleY': 1,
//     'scaleZ': 1,
//     'y': '34px',
//   }]
// };

// var ixKPlusMinusOff = {
//   'stepsA': [{
//     'opacity': 0,
//     'transition': 'transform 200ms ease 0ms, opacity 200ms ease 0ms',
//     'scaleX': 1,
//     'scaleY': 1,
//     'scaleZ': 1,
//     'y': '0px',
//   }]
// };










// $(':attr(\'^data-cart-add\')').on({
//   click: function (evt) {

//     evt.stopPropagation();
//     var el = evt.target;
//     while (!el.classList.contains('button_buy') ) {
//       el = el.parentElement;
//     }

//     var sku = el.dataset.cart;

//     // if (!cart) {
//     //   cart = new Cart();
//     // }
//     //cart.data.push([sku, 1]); //TODO: make a method
//     cart.updateGood(sku, '+');
//   }
// });

// $('#top_cart, .button_buy--maincart').on({
//   click: function (evt) {
//     addBodyNoScroll(true);
//   }
// });




$('.s_g_learn').click(function(evt) {
  addBodyNoScroll();
});



$('.xz, .x_bt_close, .xb_c, .pz, .kz, .k_top_close').click(function (evt) {
  $('.p').removeClass('shown');
  $('.k').removeClass('shown');
  removeBodyNoScroll();
});





$('.px').click(function () {
  $('.p').removeClass('shown');
  $('.k').removeClass('shown');
  removeBodyNoScroll();
});
















var s = document.createElement('script');
s.type = "text/javascript";
s.async = true;
s.src = "//static.xsolla.com/embed/paystation/1.0.7/widget.min.js";
s.addEventListener('load', function (e) {
  widget_init();
//XPayStationWidget.init(options);
}, false);
var head = document.getElementsByTagName('head')[0];
head.appendChild(s);
var access_data = {
    "settings": {
       "project_id": 22887,
       "ui": {
           "size": "medium",
           "theme": "default"
       }
   },
   "purchase": {
       "virtual_items": {
           "items": [
               {
                   "sku": "1",
                   "amount": 1
               }
           ]
       }
   }
};
var access_data_custom = {
   "settings": {
       "project_id": 22887,
       "payment_method": 1380,
       "ui": {
           "size": "medium",
           "theme": "default"
       }
   },
   "purchase": {
       "virtual_items": {
           "items": [
               {
                   "sku": "1",
                   "amount": 1
               }
           ]
       }
   }
};
function widget_init() {
   $('[data-xpaystation=widget-open-custom]').click(function(eventObject) {
       var sku = $(this).data('sku');
       access_data_custom.purchase.virtual_items.items[0].sku = sku;
       var options = {
           access_data: access_data_custom,
           lightbox: {
               width: '740px',
               height: '685px',
               spinner: 'round',
               spinnerColor: '#cccccc',
               contentBackground: "rgba(0,0,0,0.5)"
           }
       };
       XPayStationWidget.init(options);
       XPayStationWidget.open();
   });
   $('[data-xpaystation=widget-open]').click(function(eventObject) {
       var sku = $(this).data('sku');
       access_data.purchase.virtual_items.items[0].sku = sku;
       var options = {
           access_data: access_data,
           lightbox: {
               width: '740px',
               height: '685px',
               spinner: 'round',
               spinnerColor: '#cccccc',
               contentBackground: "rgba(0,0,0,0.5)"
           }
       };
       XPayStationWidget.init(options);
       XPayStationWidget.open();
   });
}
//$(window).stellar();
