if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

var container;

var delta, camera, cameraControls, scene, renderer, mesh, sphere, sphereMesh, bluelight;
var group, bg;
var texture, backgroundMesh, backgroundScene, backgroundCamera;

var clock = new THREE.Clock();

init();
animate();

function init() {
    
        // renderer

        renderer = new THREE.WebGLRenderer({antialias: true, alpha: true});
        renderer.setSize(window.innerWidth, window.innerHeight);

        container = document.getElementById('container');
        container.appendChild(renderer.domElement);

        camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 10000 );
        camera.position.z = 1;
        camera.position.y = 1.5;

        cameraControls = new THREE.TrackballControls(camera, renderer.domElement);
        cameraControls.target.set(0, 0, 0);
        cameraControls.noZoom = true;
        cameraControls.noPan = true;

        scene = new THREE.Scene();

        // lights

        light = new THREE.DirectionalLight( 0xcccccc );
        light.position.set(1, 1, 0.5);
        light.castShadow = true;
        scene.add( light );
        
        light = new THREE.DirectionalLight( 0x999999 );
        light.position.set( -1, -1, -1 );
        scene.add( light );
        
        light = new THREE.AmbientLight( 0x666666 );
        scene.add( light );
       
        material = new THREE.MeshPhongMaterial();

        material.map = THREE.ImageUtils.loadTexture('images/futurehat_tex.jpg');
        material.bumpMap = THREE.ImageUtils.loadTexture('images/futurehat_bump.jpg');
        material.bumpScale = 0.04;
        material.shininess = 0.1;

        group = new THREE.Object3D();

        //load mesh 
        var loader = new THREE.JSONLoader();
        loader.load('models/futurehat.json', modelLoadedCallback);

        window.addEventListener( 'resize', onWindowResize, false );
                setInterval (function(){
                bluelight.color.set(0x0000ff);
                sphereMesh.position.set (-0.58,-0.77,-0.19);
                setTimeout(function(){bluelight.color.set(0x000000);sphereMesh.position.set (1000,1000,1000);}, 200);
        },3000);

}


function modelLoadedCallback(geometry) {

        mesh = new THREE.Mesh( geometry, material );
        mesh.rotation.y =-0.5;
        mesh.rotation.x =0;
        mesh.rotation.z =1;
        texture = THREE.ImageUtils.loadTexture( 'images/gradient-grey.jpg');
        sphere = new THREE.SphereGeometry(0.014,0,0);
        var sphereMat = new THREE.MeshBasicMaterial({
                color:0x0000ff,
                alphaMap: texture
        });
        sphereMesh = new THREE.Mesh(sphere, sphereMat);
        sphereMesh.position.set (-0.58,-0.77,-0.19);

        bluelight = new THREE.PointLight( 0x000000 );
        bluelight.position.set(-0.58,-2,-0.19);
        sphereMesh.add(bluelight);
        mesh.add(sphereMesh);
        group.add(mesh);
        scene.add( group );

}

function onWindowResize() {

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
        
        delta = clock.getDelta();
        clock.start();
        clockTime = Math.round(clock.getElapsedTime());
        requestAnimationFrame(animate);
        
        cameraControls.update(delta);

        renderer.autoClear = false;
        renderer.clear();
        renderer.render(scene, camera);
        group.rotation.z += 0.004;


}
